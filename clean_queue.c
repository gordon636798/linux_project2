#define _GNU_SOURCE
#include <stdlib.h>
#include <sys/time.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char const *argv[])
{
    int random_num1, random_num2;
    pid_t ttid;
    int count;
    char str[300];

    struct timeval t1, t2;

    gettimeofday(&t1, NULL);
    ttid = syscall(SYS_gettid);
    //printf("%d\n", ttid);
    count=0;

    FILE *pFile;
    char ttid_str[10];
    memset(&ttid_str, 0, strlen(ttid_str));
    sprintf(ttid_str, "%d", ttid);
    //printf("%s", ttid_str);
    char file_name[16];
    memset(&file_name, 0, strlen(file_name));
    strcat(file_name, "file_");
    strcat(file_name, ttid_str);
    printf("%s\n", file_name);
    pFile = fopen(file_name, "a+");

    while(1)
    {
        gettimeofday(&t2, NULL);

        if((t2.tv_sec-t1.tv_sec) > 180){
            break;
        }

        random_num1 = (rand()%3)+1;
        random_num2= rand()%60;
        sleep(random_num2);

        struct timeval now;
        gettimeofday(&now, NULL);
        sprintf(str, "Wake up all processes in wait queue project2_queue_%d at time %ld\n", random_num1, now.tv_sec);
        fwrite(str, 1, strlen(str)+1, pFile);
        memset(&str, 0, strlen(str));

        syscall(386, random_num1);
        count++;
    }

    char* clean_msg_1 = "Clean wait wait queue project2_queue_1\n\0";
    char* clean_msg_2 = "Clean wait wait queue project2_queue_2\n\0";
    char* clean_msg_3 = "Clean wait wait queue project2_queue_3\n\0";

    fwrite(clean_msg_1, sizeof(char), strlen(clean_msg_1), pFile);
    syscall(386, 1);
    fwrite(clean_msg_2, sizeof(char), strlen(clean_msg_2), pFile);
    syscall(386, 2);
    fwrite(clean_msg_3, sizeof(char), strlen(clean_msg_3), pFile);
    syscall(386, 3);
    fclose(pFile);
    printf("process %d completes!\n", ttid);

    return 0;
}

