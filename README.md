# LINUX project2

run file ```./demo.sh```


![](https://i.imgur.com/FO7imlr.png)


## enter_queue.c
```c
#define _GNU_SOURCE
#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <pthread.h>

void* do_thread(){
    int random_num;
    pid_t ttid, count;
    char str[300];

    struct timeval t1,t2;

    gettimeofday(&t1, NULL);
    ttid = (pid_t)syscall(SYS_gettid);
    count = 0;

    FILE *pFile;
    char ttid_str[10];
    memset(&ttid, 0, strlen(ttid_str));
    sprintf(ttid_str, "%d", ttid);
    char file_name[16];
    memset(&file_name, 0, strlen(file_name));
    strcat(file_name, "file_");
    strcat(file_name, ttid_str);
    printf("%s\n", file_name);
    pFile = fopen(file_name, "a+");

    while(1){
        gettimeofday(&t2, NULL);

        if((t2.tv_sec-t1.tv_sec) > 180){
            break;
        }

        random_num = (rand()%3)+1;

        sprintf(str, "process %d is added into wait queue project2_queue_%d the %d th time at time %ld\n", ttid, random_num, count, t2.tv_sec);
        fwrite(str, 1, strlen(str)+1, pFile);
        memset(&str, 0, strlen(str));
        syscall(385, random_num);
        count++;
    }

    fclose(pFile);
    printf("process %d completes!\n", ttid);
}

int main(int argc, char const *argv[])
{
    pthread_t t1, t2, t3;
    pthread_create(&t1, NULL, do_thread, NULL);
    pthread_create(&t2, NULL, do_thread, NULL);
    pthread_create(&t3, NULL, do_thread, NULL);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    pthread_join(t3, NULL);
    return 0;
}


```

## clean_queue.c

```c
#define _GNU_SOURCE
#include <stdlib.h>
#include <sys/time.h>
#include <sys/syscall.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>

int main(int argc, char const *argv[])
{
    int random_num1, random_num2;
    pid_t ttid;
    int count;
    char str[300];

    struct timeval t1, t2;

    gettimeofday(&t1, NULL);
    ttid = syscall(SYS_gettid);
    //printf("%d\n", ttid);
    count=0;

    FILE *pFile;
    char ttid_str[10];
    memset(&ttid_str, 0, strlen(ttid_str));
    sprintf(ttid_str, "%d", ttid);
    //printf("%s", ttid_str);
    char file_name[16];
    memset(&file_name, 0, strlen(file_name));
    strcat(file_name, "file_");
    strcat(file_name, ttid_str);
    printf("%s\n", file_name);
    pFile = fopen(file_name, "a+");

    while(1)
    {
        gettimeofday(&t2, NULL);

        if((t2.tv_sec-t1.tv_sec) > 180){
            break;
        }

        random_num1 = (rand()%3)+1;
        random_num2= rand()%60;
        sleep(random_num2);

        struct timeval now;
        gettimeofday(&now, NULL);
        sprintf(str, "Wake up all processes in wait queue project2_queue_%d at time %ld\n", random_num1, now.tv_sec);
        fwrite(str, 1, strlen(str)+1, pFile);
        memset(&str, 0, strlen(str));

        syscall(386, random_num1);
        count++;
    }

    char* clean_msg_1 = "Clean wait wait queue project2_queue_1\n\0";
    char* clean_msg_2 = "Clean wait wait queue project2_queue_2\n\0";
    char* clean_msg_3 = "Clean wait wait queue project2_queue_3\n\0";

    fwrite(clean_msg_1, sizeof(char), strlen(clean_msg_1), pFile);
    syscall(386, 1);
    fwrite(clean_msg_2, sizeof(char), strlen(clean_msg_2), pFile);
    syscall(386, 2);
    fwrite(clean_msg_3, sizeof(char), strlen(clean_msg_3), pFile);
    syscall(386, 3);
    fclose(pFile);
    printf("process %d completes!\n", ttid);

    return 0;
}


```


## syscall file

```c
#include <linux/kernel.h>
#include <linux/wait.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/time.h>
#include <linux/delay.h>
#include <linux/workqueue.h>

static DECLARE_WAIT_QUEUE_HEAD(project2_queue_1);
int q1_flag, q2_flag, q3_flag;
static DECLARE_WAIT_QUEUE_HEAD(project2_queue_2);
static DECLARE_WAIT_QUEUE_HEAD(project2_queue_3);

asmlinkage int sys_enter_wait_queue(int x) {
    if( x<1 || x>3 ){
        printk("error wait_queues bounding\n");
        return 0;
    }
    printk("enter_wait_queue(%d) called\n", x);
    //printk("project_queues: %p\t%p\t%p\n", &project2_queue_1, &project2_queue_2, &project2_queue_3);
    if(x == 1){
        q1_flag = 0;
        wait_event(project2_queue_1, q1_flag == 1);
    }else if(x == 2){
        q2_flag = 0;
        wait_event(project2_queue_2, q2_flag == 1);
    }else{
        q3_flag = 0;
        wait_event(project2_queue_3, q3_flag == 1);
    }
    return 1;
}

asmlinkage int sys_clean_wait_queue(int x) {
    //printk("project_queues: %p\t%p\t%p\n", &project2_queue_1, &project2_queue_2, &project2_queue_3);
    if( x<1 || x>3 ){

        printk("error wait_queues bounding\n");
        return 0;
    }
    if(x == 1){
        q1_flag = 1;
        wake_up_all(&project2_queue_1);
    }else if(x == 2){
        q2_flag = 1;
        wake_up_all(&project2_queue_2);
    }else{
        q3_flag = 1;
        wake_up_all(&project2_queue_3);
    }
    printk("clean_wait_queues(%d) called\n", x);
    return 1;
}

```

