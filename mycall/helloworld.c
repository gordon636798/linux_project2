#include <linux/kernel.h>
#include <linux/wait.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/time.h>
#include <linux/delay.h>
#include <linux/workqueue.h>

static DECLARE_WAIT_QUEUE_HEAD(project2_queue_1);
int q1_flag, q2_flag, q3_flag;
static DECLARE_WAIT_QUEUE_HEAD(project2_queue_2);
static DECLARE_WAIT_QUEUE_HEAD(project2_queue_3);

asmlinkage int sys_enter_wait_queue(int x) {
    if( x<1 || x>3 ){
        printk("error wait_queues bounding\n");
        return 0;
    }
    printk("enter_wait_queue(%d) called\n", x);
    //printk("project_queues: %p\t%p\t%p\n", &project2_queue_1, &project2_queue_2, &project2_queue_3);
    if(x == 1){
        q1_flag = 0;
        wait_event(project2_queue_1, q1_flag == 1);
    }else if(x == 2){
        q2_flag = 0;
        wait_event(project2_queue_2, q2_flag == 1);
    }else{
        q3_flag = 0;
        wait_event(project2_queue_3, q3_flag == 1);
    }
    return 1;
}

asmlinkage int sys_clean_wait_queue(int x) {
    //printk("project_queues: %p\t%p\t%p\n", &project2_queue_1, &project2_queue_2, &project2_queue_3);
    if( x<1 || x>3 ){

        printk("error wait_queues bounding\n");
        return 0;
    }
    if(x == 1){
        q1_flag = 1;
        wake_up_all(&project2_queue_1);
    }else if(x == 2){
        q2_flag = 1;
        wake_up_all(&project2_queue_2);
    }else{
        q3_flag = 1;
        wake_up_all(&project2_queue_3);
    }
    printk("clean_wait_queues(%d) called\n", x);
    return 1;
}

