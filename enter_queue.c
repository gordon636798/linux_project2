#define _GNU_SOURCE
#include <stdlib.h>
#include <sys/time.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <pthread.h>

void* do_thread(){
    int random_num;
    pid_t ttid, count;
    char str[300];

    struct timeval t1,t2;

    gettimeofday(&t1, NULL);
    ttid = (pid_t)syscall(SYS_gettid);
    count = 0;

    FILE *pFile;
    char ttid_str[10];
    memset(&ttid, 0, strlen(ttid_str));
    sprintf(ttid_str, "%d", ttid);
    char file_name[16];
    memset(&file_name, 0, strlen(file_name));
    strcat(file_name, "file_");
    strcat(file_name, ttid_str);
    printf("%s\n", file_name);
    pFile = fopen(file_name, "a+");

    while(1){
        gettimeofday(&t2, NULL);

        if((t2.tv_sec-t1.tv_sec) > 180){
            break;
        }

        random_num = (rand()%3)+1;

        sprintf(str, "process %d is added into wait queue project2_queue_%d the %d th time at time %ld\n", ttid, random_num, count, t2.tv_sec);
        fwrite(str, 1, strlen(str)+1, pFile);
        memset(&str, 0, strlen(str));
        syscall(385, random_num);
        count++;
    }

    fclose(pFile);
    printf("process %d completes!\n", ttid);
}

int main(int argc, char const *argv[])
{
    pthread_t t1, t2, t3;
    pthread_create(&t1, NULL, do_thread, NULL);
    pthread_create(&t2, NULL, do_thread, NULL);
    pthread_create(&t3, NULL, do_thread, NULL);

    pthread_join(t1, NULL);
    pthread_join(t2, NULL);
    pthread_join(t3, NULL);
    return 0;
}

