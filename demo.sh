enter_queue.c -lpthread -o enter_queue
gcc clean_queue.c -o clean_queue
chmod +x remove_file.sh
./remove_file.sh
tmux new-session -s dev -n "linux_project2_demo" -d

tmux split-window -h -t dev:0
tmux send-keys -t dev:0.0 './enter_queue' C-m
tmux send-keys -t dev:0.1 './clean_queue' C-m
tmux split-window -v -t dev:0.0
tmux split-window -v -t dev:0.1

tmux select-window -t dev:0
tmux attach -t dev

